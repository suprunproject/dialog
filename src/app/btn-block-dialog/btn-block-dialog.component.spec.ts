import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnBlockDialogComponent } from './btn-block-dialog.component';

describe('BtnBlockDialogComponent', () => {
  let component: BtnBlockDialogComponent;
  let fixture: ComponentFixture<BtnBlockDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnBlockDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnBlockDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
