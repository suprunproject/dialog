import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlockDialogComponent } from './block-dialog/block-dialog.component';
import { BtnBlockDialogComponent } from './btn-block-dialog/btn-block-dialog.component';
import { StatisticsBlockDialogComponent } from './statistics-block-dialog/statistics-block-dialog.component';
import { ItemStatisticsBlockDialogComponent } from './item-statistics-block-dialog/item-statistics-block-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    BlockDialogComponent,
    BtnBlockDialogComponent,
    StatisticsBlockDialogComponent,
    ItemStatisticsBlockDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
