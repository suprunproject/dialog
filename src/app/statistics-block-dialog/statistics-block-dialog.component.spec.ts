import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsBlockDialogComponent } from './statistics-block-dialog.component';

describe('StatisticsBlockDialogComponent', () => {
  let component: StatisticsBlockDialogComponent;
  let fixture: ComponentFixture<StatisticsBlockDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatisticsBlockDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsBlockDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
