import { Component, OnInit } from '@angular/core';
export interface ItemInfo {
  name:string
  fullname:string
  mainname:string

  percent:number
  conversation:number | null
  coin:number | null

  class:string
  valuecoin: number | null
}
@Component({
  selector: 'app-statistics-block-dialog',
  templateUrl: './statistics-block-dialog.component.html',
  styleUrls: ['./statistics-block-dialog.component.sass']
})
export class StatisticsBlockDialogComponent implements OnInit {
  

  constructor() {
    this.costCoin(this.items);
    this.percentageCalc(this.items);
  }

  ngOnInit(): void {
  }
  items: ItemInfo[] = [
    {class:'au',name: 'AU$', mainname:'AU$',fullname:'-',percent:13.5,conversation:1333.65,coin:null,valuecoin:null},
    {class:'btc',name: 'BTC', mainname:'AU$',fullname:'BTC',percent:21,conversation:null,coin:0.0084621,valuecoin:67487.9408},
    {class:'eth',name: 'ETH', mainname:'AU$',fullname:'ETH',percent:19.7,conversation:null,coin:15.534,valuecoin:54.022},
    {class:'xpr',name: 'XPR', mainname:'AU$',fullname:'XPR',percent:12.4,conversation:null,coin:255.3,valuecoin:2.2555},
    {class:'btc_ca',name: 'BTC Ca.', mainname:'AU$',fullname:'BTC Cash',percent:10.7,conversation:null,coin:1325.354,valuecoin:0.177932},
    {class:'ltc',name: 'LTC', mainname:'AU$',fullname:'LTC',percent:8.9,conversation:null,coin:1670.423,valuecoin:0.077588},
    {class:'eos',name: 'EOS', mainname:'AU$',fullname:'EOS',percent:6.8,conversation:null,coin:8.4453,valuecoin:7.71198},
    {class:'xtz',name: 'XTZ', mainname:'AU$',fullname:'XTZ',percent:4.3,conversation:null,coin:902.7,valuecoin:0.41967},
    {class:'link',name: 'LINK', mainname:'AU$',fullname:'LINK',percent:2.6,conversation:null,coin:77.3,valuecoin:10.9822},
    {class:'dash',name: 'DASH', mainname:'AU$',fullname:'DASH',percent:2.2,conversation:null,coin:33.8967,valuecoin:3.54402},
    {class:'eth_cl',name: 'ETH Cl.', mainname:'AU$',fullname:'ETH Classic',percent:1.6,conversation:null,coin:34.5231,valuecoin:0.814237},
  ]
  total:number = 0;
  costCoin (list:ItemInfo[]): void {
    for ( let i = 0; i < list.length; i++ ) {
      if(list[i].coin !== null && list[i].valuecoin !== null) {
          // @ts-ignore
          list[i].conversation = parseFloat((list[i].valuecoin * list[i].coin).toFixed(2))
      }
      if(list[i].conversation !== null){
      // @ts-ignore
      this.total += list[i].conversation;
      }
    }
  }

  percentageCalc (list:ItemInfo[]): void {
    for ( let i = 0; i < list.length; i++ ) {
      if(list[i].conversation !== null) {
        // @ts-ignore
        list[i].percent = parseFloat((list[i].conversation * 100 / this.total).toFixed(1))
      }
    }
  }
  

}
