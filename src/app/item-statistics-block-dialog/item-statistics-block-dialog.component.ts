import { Component, Input, OnInit } from '@angular/core';
import { ItemInfo } from '../statistics-block-dialog/statistics-block-dialog.component';

@Component({
  selector: 'app-item-statistics-block-dialog',
  templateUrl: './item-statistics-block-dialog.component.html',
  styleUrls: ['./item-statistics-block-dialog.component.sass'],

})
export class ItemStatisticsBlockDialogComponent implements OnInit {

  @Input() item!:ItemInfo
  
 
  constructor() {}

  ngOnInit(): void {}
 
}

