import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemStatisticsBlockDialogComponent } from './item-statistics-block-dialog.component';

describe('ItemStatisticsBlockDialogComponent', () => {
  let component: ItemStatisticsBlockDialogComponent;
  let fixture: ComponentFixture<ItemStatisticsBlockDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemStatisticsBlockDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemStatisticsBlockDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
